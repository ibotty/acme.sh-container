FROM registry.access.redhat.com/ubi8/ubi-minimal:latest

ENV ACMESH_RELEASE=2.8.8 \
    ACMESH_SHA256SUM=5760a328b598b7152edcb97162d3c9be4adb9c866d0a3c4f8c8ebc11ebeec1fe \
    LE_WORKING_DIR="/usr/local/share/acme.sh" \
    LE_CONFIG_HOME="/home/acmesh/account"

RUN microdnf install openssl shadow-utils \
 && useradd acmesh \
 && [ "$(id -u acmesh)" -eq 1000 ] \
 && microdnf remove shadow-utils libsemanage \
 && microdnf clean all

RUN microdnf install tar gzip \
 && cd /tmp \
 && curl -sSLo acme.sh.tar.gz https://github.com/acmesh-official/acme.sh/archive/${ACMESH_RELEASE}.tar.gz \
 && echo "$ACMESH_SHA256SUM" acme.sh.tar.gz | sha256sum -c --status --strict \
 && tar xzf /tmp/acme.sh.tar.gz \
 && cd "/tmp/acme.sh-${ACMESH_RELEASE}" \
 && ./acme.sh --install --force --config-home /home/acmesh/account --home /usr/local/share/acme.sh --cert-home /home/acmesh/certs \
 && ln -s /usr/local/share/acme.sh/acme.sh /usr/local/bin \
 && chmod -R a+rX /usr/local/share/acme.sh \
 && chown -R acmesh /home/acmesh/account \
 && rm -r /tmp/acme.sh* \
 && microdnf remove tar gzip

VOLUME ["/home/acmesh/account", "/home/acmesh/certs"]
USER 1000
ENTRYPOINT ["/usr/local/share/acme.sh/acme.sh", "--config-home", "/home/acmesh/account"]
CMD ["--help"]
